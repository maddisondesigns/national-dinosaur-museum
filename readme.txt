=== National Dinosaur Museum ===
Contributors: ahortin
Requires at least: 4.1
Tested up to: 4.1
Stable tag: 1.0

The WordPress theme for National Dinosaur Museum.


== Description ==

The National Dinosaur Museum is situated in Canberra, Australia's captial city. It is one of the premiere tourist attractions in the area.

Starting from humble beginnings in 1993, the museum has grown from just a small collection to housing the largest permanent display of dinosaur and other prehistoric fossil material in Australia.

With 23 complete skeletons, and over 300 displays of individual fossils, the museum continues to grow and expand our collection and our depth of information.

== Changelog ==

= 1.0 =
- Initial commit
